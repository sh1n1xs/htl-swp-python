def bubbleSort(arr):
    n = len(arr)
    for i in range(n - 1):
        for j in range(0, n - i - 1):
            if arr[j] > arr[j + 1]:
                arr[j], arr[j + 1] = arr[j + 1], arr[j]
    return arr


def selectionSort(arr):
    for i in range(len(arr)):
        min_ = i
        for j in range(i + 1, len(arr)):
            if arr[min_] > arr[j]:
                min_ = j
        # swap
        arr[i], arr[min_] = arr[min_], arr[i]
    return arr

def inseriontSort(arr):
    for i in range(1, len(arr)):
        key = arr[i]
        j = i - 1
        while j >= 0 and key < arr[j]:
            arr[j + 1] = arr[j]
            j -= 1
        arr[j + 1] = key
    return arr

def resetArr():
    return [234,238,194,1897,294,194,1,2422,147,876,945,527]

darr = resetArr()
print("unsorted")
print(darr)

darr = resetArr()
print("insertion sort")
print(inseriontSort(darr))

darr = resetArr()
print("selection sort")
print(selectionSort(darr))

darr = resetArr()
print("bubble sort")
print(bubbleSort(darr))